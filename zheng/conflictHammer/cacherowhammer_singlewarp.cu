#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>

#include <sys/time.h>

#define SETSIZE 256 * 1024 * 1024 * 3
#define ACCRANGE 2 * 1024 * 65
#define NEW_ACCRANGE 65
#define ACCESSES 540000*10
#define TARGETS 1024
#define ITERATIONS 100000
#define ALL1  0xffffffff
#define ALL0 0x00000000
#define ALLBANKROW 32*1024 //assuming 128KB 

__global__ void random(unsigned int seed, unsigned int *result)
{
	curandState_t state;
	curand_init(seed, 0, 0, &state);
	result[threadIdx.x] = curand(&state) % (SETSIZE/1024);
	return;
}

__global__ void check(unsigned int * dataset, int N, int * flipcount)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if ( tid < N )
	{
		if ( dataset[tid] != ALL1)
		atomicAdd(&flipcount[0], 1);
	}	

}

__global__ void toggle(unsigned int * set, unsigned int randtarget, int STRIDE, unsigned int * targets, int numinarow)
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int targetaddr = randtarget*1024;
	unsigned int warpid = (threadIdx.x / 32);
	unsigned int offset;
	unsigned int i;
	unsigned int j;
	unsigned int n;
  __shared__ unsigned int setelem[65];
	


	/*for ( n = 16; n < 32; n *=2 ) {*/
	int loop1step = numinarow;
	int loop2step = 64/loop1step;

	for ( i = 0; i < loop1step; i++ )
	{
		for ( j = 0; j < loop2step; j++)
		{
			setelem[i*loop2step + j] = targetaddr + i*STRIDE +  ALLBANKROW*j*2;
		}
	}

	setelem[64] = setelem[63] + 2*ALLBANKROW;

	int m;

	/*#pragma unroll 100*/
	for ( m = 0; m < ACCESSES; m+=10)
	{
		unsigned int addr=0;

		#pragma unroll
		for ( j = 0; j < 10; j++) {

		#pragma unroll
		for ( i = 0; i < 65; i++ )
		{
				addr = setelem[i];
				temp += set[addr];
		}
		}


	}
	/*}*/

	targets[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	struct timeval t1, t2;
	double elapsedTime;

	unsigned int * H_Set = (unsigned int * )malloc(SETSIZE * sizeof(unsigned int));
	unsigned int * D_Set;

	cudaError err;
	err = cudaMalloc((void **)&D_Set, SETSIZE * sizeof(unsigned int));

	time_t t;

	srand((unsigned) time(&t));

	if( cudaSuccess != err)
	{
		printf("SET cudaMalloc Error\n");
		return 0;
	}

	unsigned int* tarIndex;
	err = cudaMalloc((void **) &tarIndex, TARGETS * sizeof(unsigned int));

	int* flipcheck;
	cudaMalloc((void**)&flipcheck, sizeof(unsigned int));

	unsigned int randnum;

	if( cudaSuccess != err)
	{
		printf("Target Addrs cudaMalloc Error\n");
		return 0;
	}

	int associativity = 8;
	int sets = 1024;
	int lines = 8;

	int strides = 1024*2;// 8KB at least to be completely safe
	int banks = 1;
	
	if(argc == 4)
	{
		associativity = atoi(argv[1]);
		sets = atoi(argv[2]);
		lines = atoi(argv[3]);
		strides = associativity * sets * lines;
	}

	if(argc == 2)
	{
		banks = atoi(argv[1]);
		strides *= banks;
	}

	for(int i = 0; i < SETSIZE; i++)
	{
		H_Set[i] = ALL1;
	}

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyHostToDevice);

	int index = -1;
	int counter;

	if ( argc != 3) printf("Usage: %s iternum numberInaRow", argv[0]);

	//quick hack, need to fixed in the future
	int iternum = atoi(argv[1]);
	int numinarow = atoi(argv[2]);

	cudaMemset(flipcheck, 0, sizeof(int));

	for(int j = 0; j < iternum; j++)
	{
		int randnum = rand()%(SETSIZE/1024 - 1024);
		printf("rand number is %d\n", randnum);
		

		/*for ( int k=0; k < 10; k++ ) {*/
		cudaThreadSynchronize();
		gettimeofday(&t1, NULL);
		toggle<<<1,1>>>(D_Set, randnum, strides, tarIndex, numinarow);
		cudaThreadSynchronize();
		gettimeofday(&t2, NULL);

		check<<<SETSIZE/1024+1,1024>>>(D_Set, SETSIZE, flipcheck); 


		elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

		printf("Elapsed Time: %lf\n", elapsedTime);
		/*}*/
		H_Set[0] = 0;
		cudaMemcpy(H_Set, flipcheck, sizeof(unsigned int), cudaMemcpyDeviceToHost);
		if ( H_Set[0] != 0 ) { printf("%d bits flipped\n", H_Set[0]); exit(1);}
		/*for(counter = 0; counter < SETSIZE; counter++)*/
		/*{*/
		/*if(H_Set[counter] != ALL0)*/
		/*{*/
		/*printf("ERROR, randnum is %d\n", randnum);*/
		/*index = counter;*/
		/*break;*/
		/*}*/
		/*}*/
		printf("iter %d out of %d done \n", j, ITERATIONS);
		if(index != -1) break;
	}

	if(index != -1)	printf("Index %d flipped, Value is %x\n", index, H_Set[index]);


	return 0;
}
