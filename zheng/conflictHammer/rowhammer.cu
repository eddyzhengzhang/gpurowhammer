#include <stdio.h>
#include <stdlib.h>

#define DATASIZE 1024 * 1024 * 33
#define NUMITER 1
#define TOGGLES 500
#define  TARGETS 32

__global__ void initData(unsigned int * dataSet) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	dataSet[tid] = 0xffffffff;
}

__global__ void toggle(unsigned int ** targetSet, int STRIDE, int NUMCACHESIZE) {
	unsigned int * targetAddr = targetSet[threadIdx.x % TARGETS];
	unsigned int sum = 0;
	unsigned int temp = 0;
	unsigned int warpid = (threadIdx.x/32);
	sum += *targetAddr + 1;

	for(int i = 0; i < TOGGLES; i++) {
		for(int j = 1; j <= NUMCACHESIZE; j++) {
			temp += *(targetAddr + STRIDE * j + 32*warpid) + 1;
		}
		
		for(int k = 1; k < NUMCACHESIZE; k++) {
			temp += *(targetAddr + STRIDE * k + 32*warpid) + 1;
		}

		sum += *targetAddr + 1;
		
		if(sum != 0){
			printf("ERROR\n");
		}

		targetSet[threadIdx.x] += temp;
	}
}

int main(int argc, const char *argv[])
{
	int STRIDE = 32;
	int NUMCACHESIZE = 16;
	if(argc == 2)
	{
		STRIDE = atoi(argv[1]);
		NUMCACHESIZE = atoi(argv[2]);
	}

	cudaDeviceReset();
	unsigned int ** targetHost = (unsigned int **)malloc(TARGETS * sizeof(unsigned int *));
	unsigned int * dataHost = (unsigned int *)malloc(DATASIZE);
	unsigned int * dataDevice;
	unsigned int ** targetDevice;

	cudaMalloc((void **)&dataDevice, DATASIZE);
	cudaMalloc((void **)&targetDevice, TARGETS * sizeof(unsigned int *));

	initData<<<DATASIZE/(256*sizeof(int)),256>>>(dataDevice);

	int numErrors = 0;
	int indexError;
	for(int i = 0; i < NUMITER; i++){
		for(int j = 0; j < TARGETS; j++){
			targetHost[j] = dataDevice + (((rand() << 12)&0x7fffffff) % DATASIZE)/sizeof(int) ;
		}

		cudaMemcpy(targetDevice, targetHost, TARGETS * sizeof(unsigned int *), cudaMemcpyHostToDevice);

		toggle<<<1,1024>>>(targetDevice, 2048, 64); 
		cudaDeviceSynchronize();

		if(i % 5 == 0 || i == NUMITER){
			cudaMemcpy(dataHost, dataDevice, DATASIZE, cudaMemcpyDeviceToHost);
			for(int k = 0; k < DATASIZE / sizeof(int); k++){
				if(dataHost[k] != 0xffffffff) {
					numErrors++;
					indexError = k;
				}
			}
		}

		if(numErrors != 0) {
			printf("ERROR, 0x%x\n", &dataHost[indexError]);
			break;
		}
	}

	return 0;
}
