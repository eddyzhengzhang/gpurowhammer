#include <stdio.h>
#include <stdlib.h>

#define ACCESSES 1024 * 1024 * 2

__global__ void cachetest(int * set, int STRIDE, unsigned int SETSIZE)
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int j = 0;

	while(counter < ACCESSES)
	{
		j %= SETSIZE;

		temp += set[j];
		j += STRIDE;
		counter++;
	}

	set[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	int association = 8;
	int sets = 1024;
	int lines = 8;
	int strides = 1;
	int N = 1024;
	
	if(argc == 4)
	{
		association = atoi(argv[1]);
		sets = atoi(argv[2]);
		lines = atoi(argv[3]);
		strides = association * sets * lines;
	}

	unsigned int SETSIZE = 1;
	SETSIZE = 256 * 2024 * (N + 1);
	strides = 128 * 1024 / N;

	if(argc == 2)
	{
		strides = atoi(argv[1]);
	}


        int * H_Set = (int * )malloc(SETSIZE * sizeof(int));
	int * D_Set;
	cudaMalloc((void **)&D_Set, SETSIZE * sizeof(int));


	for(int i = 0; i < SETSIZE; i++)
	{
		H_Set[i] = 1;
	}

	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time;

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(int), cudaMemcpyHostToDevice);

	cudaEventRecord(start,0);
	cachetest<<<1,1>>>(D_Set, strides, SETSIZE);
	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time,start,stop);

//	printf("Mem Association: %d, Cache Sets: %d, Cache Line: %d\n", association, sets, lines*4);
	printf("N = %d, SETSIZE = %dKB\n", N, (N+1)*512);
	printf("Time for the kernel: %f ms\n\n", time);
	
	return 0;
}
