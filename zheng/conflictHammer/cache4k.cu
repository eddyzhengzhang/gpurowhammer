#include <stdio.h>
#include <stdlib.h>

#define SETSIZE 256 * 4
#define ACCESSES 4096 * 128

__global__ void cachetest(int * set, int STRIDE, int NUMSET, int ROWINTER)
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	int target = 16;
	int target2 = target + ROWINTER;
	int x = 1;
	int y = 1;

	while(counter < ACCESSES)
	{
		temp += set[target];
		for(x = 1; x < NUMSET; x++)
		{
			temp += set[target + x * STRIDE];
		}
		for(y = 1; y < NUMSET - 1; y++)
		{
			temp += set[target + y * STRIDE];
		}			
		temp += set[target + NUMSET * STRIDE];


		temp += set[target2];
		for(x = 1; x < NUMSET; x++)
		{
			temp += set[target2 + x * STRIDE];
		}
		for(y = 1; y < NUMSET; y++)
		{
			temp += set[target2 + y * STRIDE];
		}
		temp += set[target2 + NUMSET * STRIDE];

		counter++;
	}

	set[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	int * H_Set = (int * )malloc(SETSIZE * sizeof(int));
	int * D_Set;
	cudaMalloc((void **)&D_Set, SETSIZE * sizeof(int));

	int strides = 1;
	int sets = 8;
	int rowinter = 16;
	
	if(argc == 4)
	{
		strides = atoi(argv[1]);
		sets = atoi(argv[2]);
		rowinter = atoi(argv[3]);
	}

	for(int i = 0; i < SETSIZE; i++)
	{
		H_Set[i] = 1;
	}

	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time;

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(int), cudaMemcpyHostToDevice);

	cudaEventRecord(start,0);
	cachetest<<<1,1>>>(D_Set, strides, sets, rowinter);
	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time,start,stop);

	printf("Stride: %d, Sets: %d\n", strides, sets);
	printf("Time for the kernel: %f ms\n\n",time);
	
	return 0;
}
