#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>

#include <sys/time.h>

#define SETSIZE 256 * 1024 * 1024 * 3
#define ACCRANGE 2 * 1024 * 65
#define NEW_ACCRANGE 65
#define ACCESSES 2000000
#define TARGETS 1024
#define ITERATIONS 100000

__global__ void random(unsigned int seed, unsigned int *result)
{
	curandState_t state;
	curand_init(seed, 0, 0, &state);
	result[threadIdx.x] = curand(&state) % (SETSIZE/1024);
	return;
}

__global__ void toggle(unsigned int * set, unsigned int randtarget, int STRIDE, unsigned int * targets)
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int targetaddr = randtarget*1024;
	unsigned int warpid = (threadIdx.x / 32);
	unsigned int offset;
	unsigned int i;
	unsigned int j;

	while(counter < ACCESSES)
	{
		offset = 0;
		unsigned int addr = targetaddr;

		#pragma unroll
		for (j = 0; j < 50; j++) {
		addr = targetaddr;

		#pragma unroll
		for ( i = 0; i < 65; i++ )
		{
				temp += set[addr];
				addr += STRIDE;
		}
		}

		
		counter += 50;
	}

	targets[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	struct timeval t1, t2;
	double elapsedTime;

	unsigned int * H_Set = (unsigned int * )malloc(SETSIZE * sizeof(unsigned int));
	unsigned int * D_Set;

	cudaError err;
	err = cudaMalloc((void **)&D_Set, SETSIZE * sizeof(unsigned int));

	time_t t;

	srand((unsigned) time(&t));

	if( cudaSuccess != err)
	{
		printf("SET cudaMalloc Error\n");
		return 0;
	}

	unsigned int* tarIndex;
	err = cudaMalloc((void **) &tarIndex, TARGETS * sizeof(unsigned int));

	unsigned int randnum;

	if( cudaSuccess != err)
	{
		printf("Target Addrs cudaMalloc Error\n");
		return 0;
	}

	int associativity = 8;
	int sets = 1024;
	int lines = 8;

	int strides = 2048;
	int banks = 1;
	
	if(argc == 4)
	{
		associativity = atoi(argv[1]);
		sets = atoi(argv[2]);
		lines = atoi(argv[3]);
		strides = associativity * sets * lines;
	}

	if(argc == 2)
	{
		banks = atoi(argv[1]);
		strides *= banks;
	}

	for(int i = 0; i < SETSIZE; i++)
	{
		H_Set[i] = 0xffffffff;
	}

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyHostToDevice);

	int index = -1;
	int counter;

	

	for(int j = 0; j < 1; j++)
	{
		int randnum = rand()%(SETSIZE/1024);
		printf("rand number is %d\n", randnum);
		
		cudaThreadSynchronize();
		gettimeofday(&t1, NULL);
		toggle<<<1,32>>>(D_Set, randnum, strides, tarIndex);
		cudaThreadSynchronize();
		gettimeofday(&t2, NULL);

		elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

		printf("Elapsed Time: %lf\n", elapsedTime);

		cudaMemcpy(H_Set, D_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyDeviceToHost);
		for(counter = 0; counter < SETSIZE; counter++)
		{
			if(H_Set[counter] != 0xffffffff)
			{
				printf("ERROR\n");
				index = counter;
				break;
			}
		}
		printf("iter %d out of %d done \n", j, ITERATIONS);
		if(index != -1) break;
	}

	if(index != -1)	printf("Index %d flipped, Value is %x\n", index, H_Set[index]);


	return 0;
}
