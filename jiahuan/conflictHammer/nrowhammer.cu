#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_profiler_api.h>

#define SETSIZE 256 * 1024 * 512
#define ACCESSES 12
#define TARGETS 32
#define ITERATIONS 1
#define ALL1 0xffffffff

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void init(unsigned int * set)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i < SETSIZE) set[i] = 0xffffffff;
	return;
}

__global__ void check(unsigned int * set, int * flipcount)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i < SETSIZE)
	{
		if(set[i] != ALL1)
		{	
			atomicAdd(&flipcount[0],1);
		}
	}
}

__global__ void random(unsigned int seed, unsigned int *result)			// Random target addresses generator
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	curandState_t state;
	curand_init(seed, i, 0, &state);
	result[i] = curand(&state) % SETSIZE;
	return;
}

__global__ void toggle(unsigned int * set, unsigned int* targets, int CMstride, int Wstride, int iternum)			// Rowhammer kernel
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int targetaddr;
	unsigned int oriaddr;
//	curandState_t state;


	/*if(threadIdx.x % 32 != 0) return;		// One thread per warp*/

	int offset = blockDim.x * blockIdx.x + threadIdx.x;
	offset /= 32;
	targetaddr = targets[0];
//	targetaddr = curand(&state) %SETSIZE;
//	targetaddr += Wstride * offset;
//	if(targetaddr + CMstride * 5 > SETSIZE)
//		targetaddr -= (CMstride*5);
	oriaddr = targetaddr;
//	unsigned int stride = CMstride * offset;



	while(counter < iternum)			// Each warp access 65 addresses in one cache set
	{
		targetaddr = oriaddr;
		temp += counter*set[targetaddr];

		/*printf("targetaddr %d\n", targetaddr);*/

		#pragma unroll		
		for(int x = 0; x < 32; x++)
		{
			targetaddr += CMstride;
			temp += set[targetaddr];
			/*printf("targetaddr %d\n", targetaddr);*/
//			temp += set[targetaddr+CMstride];		// Second access pattern
//			temp += set[targetaddr+CMstride * 32];
//			targetaddr += CMstride;
		}
		
		counter++;
	}

	targets[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	unsigned int * H_Set = (unsigned int * )malloc(SETSIZE * sizeof(unsigned int));
	unsigned int * D_Set;

	cudaError err;
	err = cudaMalloc((void **)&D_Set, SETSIZE * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("SET cudaMalloc Error\n");
		printf("%s\n", err);
		return 0;
	}

	unsigned int * H_tarIndex = (unsigned int *)malloc(TARGETS * sizeof(unsigned int));
	unsigned int * tarIndex;
	err = cudaMalloc((void **) &tarIndex, TARGETS * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("Target Addrs cudaMalloc Error\n");
		return 0;
	}

	int CMstride = 512;			// Stride within a warp
	int Wstride = 512 * 32;			// Stride between warps

	if(argc == 2)				// 1 argument, run ./a.out <CMstride>
	{
		CMstride = atoi(argv[1]);
	}

	if(argc == 3)				// 2 arguments; run ./a.out <CMstride> <Wstride>
	{
		CMstride = atoi(argv[1]);
		Wstride = atoi(argv[2]);
	}


//	int index = -1;
	unsigned int seed;
	srand((unsigned)time(NULL));

	int * flipcheck;
	err = cudaMalloc((void**)&flipcheck, sizeof(int));
	if(err != cudaSuccess)
	{
		printf("Flip check Error\n");
		return 0;
	}
	
	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time, total;
	total = 0;
//	int counter = 0;


	init<<<SETSIZE/1024,1024>>>(D_Set);	// initialize data

	for(int j = 0; j < ITERATIONS; j++)
	{
//		for(counter = 0; counter < TARGETS; counter++)
//		{
//			H_tarIndex[counter] = rand() % SETSIZE;
//		}
//		cudaMemcpy(tarIndex, H_tarIndex, TARGETS * sizeof(unsigned int), cudaMemcpyHostToDevice);

		seed = rand();
		random<<<1,TARGETS>>>(seed, tarIndex);

		printf("Running nrowhammer now\n");
seed = 1000;
		cudaEventRecord(start,0);
		toggle<<<1,1>>>(D_Set, tarIndex, CMstride, Wstride, seed);
		cudaEventRecord(stop,0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&time,start,stop);
		total += time;

		cudaMemset(flipcheck, 0, sizeof(int));
		check<<<SETSIZE/1024+1,1024>>>(D_Set, flipcheck);
		H_Set[0] = 0;
		cudaMemcpy(H_Set,flipcheck,sizeof(unsigned int),cudaMemcpyDeviceToHost);
		if(H_Set[0] != 0)
		{
			printf("%d Bit flipped\n", H_Set[0]);
			exit(1);
		}

//		cudaMemcpy(H_Set, D_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyDeviceToHost);
//		for(counter = 0; counter < SETSIZE; counter++)
//		{
//			if(H_Set[counter] != 0xffffffff)
//			{
//				printf("ERROR\n");
//				index = counter;
//				break;
//			}
//		}
//		if(index != -1) break;
	}

//	if(index != -1)	printf("Index %d flipped, Value is %x\n", index, H_Set[index]);

//	printf("NumBanks: %d, BytePerBank: %d, RowPerBank: %d, Wstride: %d\n", NumBanks, BytePerBank*4, RowPerBank, Wstride);
	printf("CMstride %d, Wstride %d\n", CMstride, Wstride);
	printf("Time for kernel: %f ms\n", total);

	cudaProfilerStop();
	return 0;
}
