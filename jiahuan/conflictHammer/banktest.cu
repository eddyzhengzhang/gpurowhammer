#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_profiler_api.h>

#define SETSIZE 256 * 1024 * 1024 * 2
#define ACCESSES 540000
#define TARGETS 32
#define ITERATIONS 1

__global__ void init(unsigned int * set)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	set[i] = 0xffffffff;
	return;
}

__global__ void random(unsigned int seed, unsigned int *result)			// Random target addresses generator
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	curandState_t state;
	curand_init(seed, i, 0, &state);
	result[i] = curand(&state) % SETSIZE;
	return;
}

__global__ void toggle(unsigned int * set, unsigned int* targets, int CMstride, int Wstride)			// Rowhammer kernel
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int targetaddr = targets[1];
	unsigned int oriaddr;

	if(threadIdx.x % 32 != 0) return;		// One thread per warp

	int offset = blockDim.x * blockIdx.x + threadIdx.x;
	offset /= 32;
	targetaddr += Wstride * offset;
	oriaddr = targetaddr;


	while(counter < ACCESSES)			// Each warp access 65 addresses in one cache set
	{
		targetaddr = oriaddr;
		temp += set[targetaddr] + 1;

		#pragma unroll		
		for(int x = 1; x < 65; x++)
		{
			temp += set[targetaddr += CMstride] + 1;
		}
		
		counter++;
	}

	targets[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	unsigned int * H_Set = (unsigned int * )malloc(SETSIZE * sizeof(unsigned int));
	unsigned int * D_Set;

	cudaError err;
	err = cudaMalloc((void **)&D_Set, SETSIZE * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("SET cudaMalloc Error\n");
		printf("%s\n", err);
		return 0;
	}

	unsigned int* tarIndex;
	err = cudaMalloc((void **) &tarIndex, TARGETS * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("Target Addrs cudaMalloc Error\n");
		return 0;
	}

	int CMstride = 2048;			// Stride within a warp
	int Wstride = 8;			// Stride between warps

	int NumBanks = 16;
	int BytePerBank = 32;
	int RowPerBank = 1;
	
	if(argc == 3)				// 2 arguments; run ./gpubank <NumBanks> <BytePerBank>
	{
		NumBanks = atoi(argv[1]);
		BytePerBank = atoi(argv[2]);

		Wstride = NumBanks * BytePerBank * RowPerBank;
	}

	if(argc == 4)				// 3 arguments; run ./gpubank <NumBanks> <BytePerBank> <RowPerBank>
	{
		NumBanks = atoi(argv[1]);
		BytePerBank = atoi(argv[2]);
		RowPerBank = atoi(argv[3]);

		Wstride = NumBanks * BytePerBank * RowPerBank;
	}
		

	int index = -1;
	int counter;
	unsigned int seed;
	srand((unsigned)time(NULL));

	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time, total;
	total = 0;


	init<<<SETSIZE/1024,1024>>>(D_Set);	// initialize data

	for(int j = 0; j < ITERATIONS; j++)
	{
		seed = rand();
		random<<<1,TARGETS>>>(seed, tarIndex);

		cudaEventRecord(start,0);
		toggle<<<1,640>>>(D_Set, tarIndex, CMstride, Wstride);
		cudaEventRecord(stop,0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&time,start,stop);
		total += time;

		cudaMemcpy(H_Set, D_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyDeviceToHost);
		for(counter = 0; counter < SETSIZE; counter++)
		{
			if(H_Set[counter] != 0xffffffff)
			{
				printf("ERROR\n");
				index = counter;
				break;
			}
		}
		if(index != -1) break;
	}

	if(index != -1)	printf("Index %d flipped, Value is %x\n", index, H_Set[index]);

	printf("NumBanks: %d, BytePerBank: %d, RowPerBank: %d, Wstride: %d\n", NumBanks, BytePerBank, RowPerBank, Wstride);
	printf("Time for kernel: %f ms\n", total);

	cudaProfilerStop();
	return 0;
}
