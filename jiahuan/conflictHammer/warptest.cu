#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_profiler_api.h>

#define SETSIZE 256 * 1024 * 1024 * 2
#define ACCRANGE 256 * 1024 * 33
#define ACCESSES 540000
#define TARGETS 32
#define ITERATIONS 1000

__global__ void random(unsigned int seed, unsigned int *result)			// Random target addresses generator
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	curandState_t state;
	curand_init(seed, i, 0, &state);
	result[i] = curand(&state) % SETSIZE;
	return;
}

__global__ void toggle(unsigned int * set, unsigned int* targets, int STRIDE, int BANKINTER)			// Rowhammer kernel
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int targetaddr = targets[1];
	unsigned int oriaddr;

	if(threadIdx.x % 32 != 0) return;

	int offset = blockDim.x * blockIdx.x + threadIdx.x;
	offset /= 32;
	targetaddr += BANKINTER * offset;
	oriaddr = targetaddr;

//	if(offset < 65)
//	{	
//		targetaddr = targetaddr + STRIDE * offset;
//	}
//	else if(offset < 130)
//	if(offset > 128)
//	{
//		offset++;
//		targetaddr = targetaddr + BANKINTER + STRIDE * (offset % 65);
//		return;
//	}
//	else if(offset < 195)
//	{
//		offset += 2;
//		targetaddr = targetaddr + BANKINTER*2 + STRIDE * (offset % 65);
//		return;
//	}
//	else if(offset < 260)
//	{
//		offset += 3;
//		targetaddr = targetaddr + BANKINTER*3 + STRIDE * (offset % 65);
//	}


	while(counter < ACCESSES)			// Each warp access 65 addresses in one cache set
	{
		targetaddr = oriaddr;
		temp += set[targetaddr] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		temp += set[targetaddr += STRIDE] + 1;
		counter++;
	}

	targets[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	unsigned int * H_Set = (unsigned int * )malloc(SETSIZE * sizeof(unsigned int));
	unsigned int * D_Set;

	cudaError err;
	err = cudaMalloc((void **)&D_Set, SETSIZE * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("SET cudaMalloc Error\n");
		printf("%s\n", err);
		return 0;
	}

	unsigned int* tarIndex;
	err = cudaMalloc((void **) &tarIndex, TARGETS * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("Target Addrs cudaMalloc Error\n");
		return 0;
	}

	int associativity = 8;
	int sets = 1024;
	int lines = 8;

	int strides = 2048;			// Stride within a warp
	int banks = 8				// Stride between warps
	
	if(argc == 4)
	{
		associativity = atoi(argv[1]);
		sets = atoi(argv[2]);
		lines = atoi(argv[3]);
		strides = associativity * sets * lines;
	}

	if(argc == 2)
	{
		banks = atoi(argv[1]);
		strides *= banks;
	}

	for(int i = 0; i < SETSIZE; i++)
	{
		H_Set[i] = 0xffffffff;
	}

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyHostToDevice);

	int index = -1;
	int counter;
	unsigned int seed;
	srand((unsigned)time(NULL));

	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time, total;
	total = 0;

	for(int j = 0; j < ITERATIONS; j++)
	{
		seed = rand();
		random<<<1,TARGETS>>>(seed, tarIndex);

		cudaEventRecord(start,0);
		toggle<<<1,512>>>(D_Set, tarIndex, strides, banks);
		cudaEventRecord(stop,0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&time,start,stop);
		total += time;

		cudaMemcpy(H_Set, D_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyDeviceToHost);
		for(counter = 0; counter < SETSIZE; counter++)
		{
			if(H_Set[counter] != 0xffffffff)
			{
				printf("ERROR\n");
				index = counter;
				break;
			}
		}
		if(index != -1) break;
	}

	if(index != -1)	printf("Index %d flipped, Value is %x\n", index, H_Set[index]);

	printf("Time for kernel: %f ms\n", total);

	cudaProfilerStop();
	return 0;
}
