#include <stdio.h>
#include <stdlib.h>

#define ACCESSES 100
//#define SETSIZE 256 * 1024 * 1024

__global__ void init(int * set, unsigned int SETSIZE)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i < SETSIZE) set[i] = 0x0;
	return;
}

__global__ void checker(int * set, int STRIDE, int N, int * errcheck)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i < N)
	{
		if(set[i*STRIDE] != ACCESSES)
			atomicAdd(&errcheck[0],1);
		__syncthreads();
	}
}

__global__ void cachetest(int * set, int STRIDE, int N, int * errcheck)
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int j = 0;
	int i;

	while(counter < ACCESSES)
	{
		j = 0;
		#pragma unroll
		for(i=0; i<N; i++)
		{
			temp += set[j]++;
			j += STRIDE;
			//printf("%d\n", (c2-c1));
			//printf("%ld\n",&set[j]);
		}
		counter++;
	}

	errcheck[1] = temp;

	return;
}

int main(int argc, char *argv[])
{
	int strides = 128 * 1024;
	int N = 1;
	unsigned int SETSIZE = 128 * 1024 * N;
	
	if(argc == 2)
	{
		N = atoi(argv[1]);
		N++;
		SETSIZE *= N;
	}

	if(argc == 3)
	{
		N = atoi(argv[1]);
		strides = atoi(argv[2]);
		N++;
		SETSIZE *= N;
	}

	unsigned int * check = (unsigned int *)malloc(2 * sizeof(unsigned int));
	int * D_Set;
	cudaError err;
	err=cudaMalloc((void **)&D_Set, SETSIZE * sizeof(int));

	if(err != cudaSuccess)
	{
		printf("Error\n");
		return 0;
	}

	int * errcheck;
	err = cudaMalloc((void **)&errcheck, 2 * sizeof(int));

	if(err != cudaSuccess)
	{
		printf("Error\n");
		return 0;
	}

	cudaMemset(errcheck, 0, sizeof(int));

	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time;

	init<<<SETSIZE/512+1,512>>>(D_Set, SETSIZE);
	cudaEventRecord(start,0);
	cachetest<<<1,1>>>(D_Set, strides, N, errcheck);
	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time,start,stop);

	checker<<<1,512>>>(D_Set, strides, N, errcheck);

	cudaMemcpy(check, errcheck, 2 * sizeof(unsigned int),cudaMemcpyDeviceToHost);

//	printf("Mem Association: %d, Cache Sets: %d, Cache Line: %d\n", association, sets, lines*4);
	printf("N = %d, Error check = %d, temp = %d\n", --N, check[0], check[1]);
	printf("Time for the kernel: %f ms\n\n", time);
	
	return 0;
}
