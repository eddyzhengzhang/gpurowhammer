#include <stdio.h>
#include <stdlib.h>

//#define ACCESSES 4096 * 512
#define ACCESSES 100000

__global__ void cachetest(int * set, int STRIDE, unsigned int SETSIZE) {
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int j = 0;
	unsigned int j2 = 0;

	while (counter < ACCESSES) {
		j = 0;
		j2 = 0;

		// NOTE: counter is not really counting the number of accesses right now.. just the number of times through the loop

		while (j < SETSIZE) {
			if (j2 == 6    || j2 == 18  || j2 == 103  || j2 == 115 || 
			    j2 == 141  || j2 == 153 || j2 == 238  || j2 == 250 || 
			    j2 == 261  || j2 == 273 || j2 == 358  || j2 == 370 ||
			    j2 == 396  || j2 == 408 || j2 == 493  || j2 == 505 ||
			    j2 == 516  || j2 == 528 || j2 == 613  || j2 == 625 ||
			    j2 == 655  || j2 == 667 || j2 == 748  || j2 == 760 || 
			    j2 == 775  || j2 == 787 || j2 == 868  || j2 == 880 || 
			    j2 == 906  || j2 == 918 || j2 == 1003 || j2 == 1015) 
			{
			//	temp += set[j];
			}
			else if (j2 == 7    || j2 == 19   || j2 == 100  || j2 == 112 || 
				 j2 == 142  || j2 == 154  || j2 == 239  || j2 == 251 || 
				 j2 == 262  || j2 == 274  || j2 == 359  || j2 == 371 ||
				 j2 == 397  || j2 == 409  || j2 == 494  || j2 == 506 || 
				 j2 == 517  || j2 == 529  || j2 == 614  || j2 == 626 ||
				 j2 == 652  || j2 == 664  || j2 == 749  || j2 == 761 ||
				 j2 == 772  || j2 == 784  || j2 == 869  || j2 == 881 ||
				 j2 == 907  || j2 == 919  || j2 == 1000 || j2 == 1012)
			{
			//	temp += set[j];
			}
			else if (j2 == 4    || j2 == 16  || j2 == 101  || j2 == 113 || 
				 j2 == 143  || j2 == 155 || j2 == 236  || j2 == 248 || 
				 j2 == 263  || j2 == 275 || j2 == 356  || j2 == 368 ||
				 j2 == 398  || j2 == 410 || j2 == 495  || j2 == 507 ||
				 j2 == 518  || j2 == 530 || j2 == 615  || j2 == 627 ||
				 j2 == 653  || j2 == 665 || j2 == 750  || j2 == 762 ||
				 j2 == 773  || j2 == 785 || j2 == 870  || j2 == 882 ||
				 j2 == 904  || j2 == 916 || j2 == 1001 || j2 == 1013)
			{
			//	temp += set[j];
			}
			else if (j2 == 5   || j2 == 17  || j2 == 102  || j2 == 114 || 
				 j2 == 140 || j2 == 152 || j2 == 237  || j2 == 249 || 
				 j2 == 260 || j2 == 272 || j2 == 357  || j2 == 369 || 
				 j2 == 399 || j2 == 411 || j2 == 492  || j2 == 504 || 
				 j2 == 519 || j2 == 531 || j2 == 612  || j2 == 624 ||
				 j2 == 654 || j2 == 666 || j2 == 751  || j2 == 763 ||
				 j2 == 774 || j2 == 786 || j2 == 871  || j2 == 883 ||
				 j2 == 905 || j2 == 917 || j2 == 1002 || j2 == 1014) 
			{	
			//	temp += set[j];
			}
			// Set note: Seems like the pattern of 12 dies in the row starting with 898
			else if (j2 == 14  || j2 == 26  || j2 == 111 || j2 == 123 ||
				 j2 == 133 || j2 == 145 || j2 == 230 || j2 == 242 ||
				 j2 == 269 || j2 == 281 || j2 == 366 || j2 == 378 ||
				 j2 == 388 || j2 == 400 || j2 == 485 || j2 == 497 ||
				 j2 == 524 || j2 == 536 || j2 == 621 || j2 == 633 ||
				 j2 == 647 || j2 == 659 || j2 == 740 || j2 == 752 ||
				 j2 == 783 || j2 == 795 || j2 == 876 || j2 == 888 ||
				 j2 == 898 || j2 == 926 || j2 == 995) 

			{
			//	temp += set[j];
			}
			else if (j2 == 15  || j2 == 27  || j2 == 108 || j2 == 120 ||
				 j2 == 134 || j2 == 146 || j2 == 231 || j2 == 243 ||
				 j2 == 270 || j2 == 282 || j2 == 367 || j2 == 379 ||
				 j2 == 389 || j2 == 401 || j2 == 486 || j2 == 498 ||
				 j2 == 525 || j2 == 537 || j2 == 622 || j2 == 634 ||
				 j2 == 644 || j2 == 656 || j2 == 741 || j2 == 753 ||
				 j2 == 780 || j2 == 792 || j2 == 877 || j2 == 889 ||
				 j2 == 899 || j2 == 927 || j2 == 992)
			{
			//	temp += set[j];
			}
			else if (j2 == 13  || j2 == 25  || j2 == 110 || j2 == 122 ||
				 j2 == 132 || j2 == 144 || j2 == 229 || j2 == 241 ||
				 j2 == 268 || j2 == 280 || j2 == 365 || j2 == 377 ||
				 j2 == 391 || j2 == 403 || j2 == 484 || j2 == 496 ||
				 j2 == 527 || j2 == 539 || j2 == 620 || j2 == 632 ||
				 j2 == 646 || j2 == 658 || j2 == 743 || j2 == 755 ||
				 j2 == 782 || j2 == 794 || j2 == 879 || j2 == 891 ||
				 j2 == 897 || j2 == 925 || j2 == 994)
			{ 
			//	temp += set[j];
			}
			else if (j2 == 12  || j2 == 24  || j2 == 109 || j2 == 121 ||
				 j2 == 135 || j2 == 147 || j2 == 228 || j2 == 240 ||
				 j2 == 271 || j2 == 283 || j2 == 364 || j2 == 376 ||
				 j2 == 390 || j2 == 402 || j2 == 487 || j2 == 499 ||
				 j2 == 526 || j2 == 538 || j2 == 623 || j2 == 635 ||
				 j2 == 645 || j2 == 657 || j2 == 742 || j2 == 754 ||
				 j2 == 781 || j2 == 793 || j2 == 878 || j2 == 890 ||
				 j2 == 896 || j2 == 924 || j2 == 993)
			{
			//	temp += set[j];
			}
			// Set note: Only 16 required numbers
			else if (j2 == 37  || j2 == 49  || j2 == 172 || j2 == 184 ||
				 j2 == 292 || j2 == 304 || j2 == 431 || j2 == 443 ||
				 j2 == 551 || j2 == 563 || j2 == 686 || j2 == 698 ||
				 j2 == 806 || j2 == 818 || j2 == 937 || j2 == 949)
			{
			//	temp += set[j];
			}
			// Set note: Only 16 required numbers
			else if (j2 == 76  || j2 == 88  || j2 == 199 || j2 == 211 ||
				 j2 == 335 || j2 == 347 || j2 == 454 || j2 == 466 ||
				 j2 == 590 || j2 == 602 || j2 == 709 || j2 == 721 ||
				 j2 == 845 || j2 == 857 || j2 == 960 || j2 == 988) 
			{
			//	temp += set[j];
			}
			// Set note: Seems to be no pattern of 12
			else if (j2 == 8   || j2 == 105 || j2 == 131 || j2 == 224  ||
				 j2 == 267 || j2 == 360 || j2 == 386 || j2 == 483  ||
				 j2 == 522 || j2 == 619 || j2 == 641 || j2 == 738  ||
				 j2 == 777 || j2 == 874 || j2 == 908 || j2 == 1005 ||
				 j2 == 1028) 
			{
			//	temp += set[j];
			}
			else if (j2 == 9   || j2 == 106 || j2 == 128 || j2 == 225  ||
				 j2 == 264 || j2 == 361 || j2 == 387 || j2 == 480  ||
				 j2 == 523 || j2 == 616 || j2 == 642 || j2 == 739  ||
				 j2 == 778 || j2 == 875 || j2 == 909 || j2 == 1006 ||
				 j2 == 1029)
			{
			//	temp += set[j];
			}
			else if (j2 == 10  || j2 == 107 || j2 == 129 || j2 == 226  ||
				 j2 == 265 || j2 == 362 || j2 == 384 || j2 == 481  ||
				 j2 == 520 || j2 == 617 || j2 == 643 || j2 == 736  ||
				 j2 == 779 || j2 == 872 || j2 == 910 || j2 == 1007 || 
				 j2 == 1030) 
			{
			//	temp += set[j];
			}
			else if (j2 == 11  || j2 == 104 || j2 == 130 || j2 == 227  ||
				 j2 == 266 || j2 == 363 || j2 == 385 || j2 == 482  ||
				 j2 == 521 || j2 == 618 || j2 == 640 || j2 == 737  ||
				 j2 == 776 || j2 == 873 || j2 == 911 || j2 == 1004 ||
				 j2 == 1031)
			{
			//	temp += set[j];
			}
			else if (j2 == 0   || j2 == 97  || j2 == 139 || j2 == 232 ||
				 j2 == 259 || j2 == 352 || j2 == 394 || j2 == 491 ||
				 j2 == 514 || j2 == 611 || j2 == 649 || j2 == 746 ||
				 j2 == 769 || j2 == 866 || j2 == 900 || j2 == 997 ||
				 j2 == 1036)
			{
				temp += set[j];
			}
			else
			{
			//	if(j2 > 100)
			//		temp += set[j];
			}
				
			j += STRIDE;
			j2++;
			counter++;
		}
	}

	set[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{	
	int association = 8;
	int sets = 1024;
	int lines = 8;
	int strides = 1;
	int N = 1;
	
	if(argc == 4)
	{
		association = atoi(argv[1]);
		sets = atoi(argv[2]);
		lines = atoi(argv[3]);
		strides = association * sets * lines;
	}

	if(argc == 2)
	{
		N = atoi(argv[1]);
	}


	unsigned int SETSIZE;
	/*strides = 128 * 1024 / N;*/
	/*strides = 2 * 1024;*/
	strides = 512;
	SETSIZE = strides * (N +1);

        int * H_Set = (int * )malloc(SETSIZE * sizeof(int));
	int * D_Set;
	cudaMalloc((void **)&D_Set, SETSIZE * sizeof(int));

	printf("%p\n\n", D_Set);

	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float time;

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(int), cudaMemcpyHostToDevice);
	

	cudaEventRecord(start,0);
	cachetest<<<1,1>>>(D_Set, strides, SETSIZE);
	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time,start,stop);

//	printf("Mem Association: %d, Cache Sets: %d, Cache Line: %d\n", association, sets, lines*4);
	printf("N = %d, SETSIZE = %dKB\n", N, (N+1)*512);
	printf("Time for the kernel: %f ms\n\n", time);
	
	return 0;
}
