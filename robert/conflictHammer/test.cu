#include <stdio.h>
#include <stdlib.h>

__global__ void kernel()
{
	printf("Thread %d is printing.\n", threadIdx.x);
	printf("Unsigned Int is: %d, pointer is %d\n", sizeof(unsigned int), sizeof(unsigned int *));
	return;
}

int main()
{
	kernel<<<1,1>>>();
	cudaDeviceSynchronize();
	
	return 0;
}
