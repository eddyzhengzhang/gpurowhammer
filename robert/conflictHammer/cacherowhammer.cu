#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>

#define SETSIZE 256 * 1024 * 1024 * 3
#define ACCRANGE 256 * 1024 * 33
#define ACCESSES 540000
#define TARGETS 32
#define ITERATIONS 100000

__global__ void random(unsigned int seed, unsigned int *result)
{
	curandState_t state;
	curand_init(seed, 0, 0, &state);
	result[threadIdx.x] = curand(&state) % SETSIZE;
	return;
}

__global__ void toggle(unsigned int * set, unsigned int* targets, int STRIDE)
{
	unsigned int temp = 0;
	unsigned int counter = 0;
	unsigned int targetaddr = targets[threadIdx.x % TARGETS];
	unsigned int offset;

	while(counter < ACCESSES)
	{
		offset = 0;

		while(offset < ACCRANGE)
		{
			temp += set[targetaddr + offset] + 1;
			offset += STRIDE;
			counter++;
		}
	}

	targets[threadIdx.x] = temp;

	return;
}

int main(int argc, char *argv[])
{
	unsigned int * H_Set = (unsigned int * )malloc(SETSIZE * sizeof(unsigned int));
	unsigned int * D_Set;

	cudaError err;
	err = cudaMalloc((void **)&D_Set, SETSIZE * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("SET cudaMalloc Error\n");
		return 0;
	}

	unsigned int* tarIndex;
	err = cudaMalloc((void **) &tarIndex, TARGETS * sizeof(unsigned int));

	if( cudaSuccess != err)
	{
		printf("Target Addrs cudaMalloc Error\n");
		return 0;
	}

	int associativity = 8;
	int sets = 1024;
	int lines = 8;

	int strides = 2048;
	int banks = 1;
	
	if(argc == 4)
	{
		associativity = atoi(argv[1]);
		sets = atoi(argv[2]);
		lines = atoi(argv[3]);
		strides = associativity * sets * lines;
	}

	if(argc == 2)
	{
		banks = atoi(argv[1]);
		strides *= banks;
	}

	for(int i = 0; i < SETSIZE; i++)
	{
		H_Set[i] = 0xffffffff;
	}

	cudaMemcpy(D_Set, H_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyHostToDevice);

	int index = -1;
	int counter;

	for(int j = 0; j < ITERATIONS; j++)
	{
		random<<<1,32>>>(time(NULL), tarIndex);
		toggle<<<1,1>>>(D_Set, tarIndex, strides);

		cudaMemcpy(H_Set, D_Set, SETSIZE * sizeof(unsigned int), cudaMemcpyDeviceToHost);
		for(counter = 0; counter < SETSIZE; counter++)
		{
			if(H_Set[counter] != 0xffffffff)
			{
				printf("ERROR\n");
				index = counter;
				break;
			}
		}
		if(index != -1) break;
	}

	if(index != -1)	printf("Index %d flipped, Value is %x\n", index, H_Set[index]);


	return 0;
}
